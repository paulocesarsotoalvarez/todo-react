import React, { Component } from 'react';

class Cards extends Component {

    constructor() {
        super();
        this.state = {
          todos
        }
        this.handleAddTodo = this.handleAddTodo.bind(this);
    }
      
    handleAddTodo(todo) {
        this.setState({
          todos: [...this.state.todos, todo]
        });
    }
      
    handleRemoveToDo(index) {
        console.log(index);
        if (window.confirm('Estas seguro de eliminar la tarea ?')) {
          this.setState({
            todos: this.state.todos.filter((e, i) => {
              return i !== index;
            })
          });
        }
    }

    render() {
        const todos = this.state.todos.map((todo, i) => {
          return(
            <div className="col-md-4" key={i}>
              <div className="card mt-4">
                <div className="card-header">
                  <h3>{todo.titulo}</h3>
                  <span className="badge badge-pill badge-danger ml-2">
                    {todo.prioridad}
                  </span>
                </div>
                <div className="card-body">
                  <p>{todo.descripcion}</p>
                  <p><mark>{todo.responsable}</mark></p>
                </div>
                <div className="card-footer">
                  <button className="btn btn-danger" onClick={this.handleRemoveToDo.bind(this, i)}>
                  Eliminar
                  </button>
                </div>
              </div>
            </div>
          )
        };
    }
}

export default Cards;