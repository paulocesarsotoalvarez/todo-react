# use a node base image
FROM node:8-alpine

# set the maintainer
LABEL maintainer "iguala@gmail.com"

# set a health check
HEALTHCHECK --interval=5s \
            --timeout=5s \
            CMD curl -f http://127.0.0.1:3000 || exit 1

# Setting up the directory App for the build process
WORKDIR "/app"
# Start build Docker container
COPY ./package.json ./package-lock.json ./
RUN npm install
RUN npm audit fix --force
COPY . .

# Entrypoint
CMD ["npm", "run", "start"]

# Epose port
EXPOSE 3000