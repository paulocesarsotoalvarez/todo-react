import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navigation from './components/Navigation';
import ToDoForm from './components/ToDoForm';

import { todos } from './todos.json';

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos
    }
    this.handleAddTodo = this.handleAddTodo.bind(this);
  }
  
  handleAddTodo(todo) {
    this.setState({
      todos: [...this.state.todos, todo]
    });
  }
  handleRemoveToDo(index) {
    console.log(index);
    if (window.confirm('Estas seguro de eliminar la tarea ?')) {
      this.setState({
        todos: this.state.todos.filter((e, i) => {
          return i !== index;
        })
      });
    }
  }

  render() {
    const todos = this.state.todos.map((todo, i) => {
      return(
        <div className="col-md-4" key={i}>
          <div className="card mt-4">
            <div className="card-header">
              <h3>{todo.titulo}</h3>
              <span className="badge badge-pill badge-danger ml-2">
                {todo.prioridad}
              </span>
            </div>
            <div className="card-body">
              <p>{todo.descripcion}</p>
              <p><mark>{todo.responsable}</mark></p>
            </div>
            <div className="card-footer">
              <button className="btn btn-danger" onClick={this.handleRemoveToDo.bind(this, i)}>
              Eliminar
              </button>
            </div>
          </div>
        </div>
      )
    });
    return (
      <div className="App"> 
        <Navigation titulo="Tareas" tasks={this.state.todos.length}/>

        <div className="container">
          <div className="row mt-4">
            
            <div className="col-md-4 text-center">
              <img src={logo} className="App-logo" alt="logo" />
              <ToDoForm onAddTodo={this.handleAddTodo}/>
            </div>

            <div className="col-md-8">
              <div className="row">
                { todos }
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default App;
