import React, { Component } from 'react';

class ToDoForm extends Component {

    constructor() {
        super();
        this.state = {
            titulo: '',
            responsable: '',
            descripcion: '',
            prioridad: 'baja'
        };
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit= this.handleSubmit.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.onAddTodo(this.state);
        console.log(this.state);
        console.log('enviando...');
    }

    handleInput(e) {
        const {value, name} = e.target;
        //console.log(value, name);
        this.setState({
            [name]: value
        });
        //console.log(this.state);
    }

    render() {
        return (
            <div className="card">
                <form className="card-body" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input 
                            type="text"
                            name="titulo"
                            placeholder="Titulo"
                            value={this.state.titulo}
                            onChange={this.handleInput}
                            className="form-control"
                        />
                    </div>
                    <div className="form-group">
                        <input 
                            type="text"
                            name="responsable"
                            value={this.state.responsable}
                            onChange={this.handleInput}
                            placeholder="Responsable"
                            className="form-control"
                        />
                    </div>
                    <div className="form-group">
                    <input 
                        type="text"
                        name="descripcion"
                        placeholder="Descripcion"
                        value={this.state.descripcion}
                        onChange={this.handleInput}
                        className="form-control"
                        />
                    </div>
                    <div className="form-group">
                        <select
                            name="prioridad"
                            value={this.state.prioridad}
                            onChange={this.handleInput}
                            className="form-control"
                        >
                            <option>baja</option>
                            <option>media</option>
                            <option>alta</option>
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Agregar Tarea
                    </button>
                </form>
            </div>
        )
    }
}

export default ToDoForm;